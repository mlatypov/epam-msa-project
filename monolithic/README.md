# Прототип в виде монолитного модуля

## Модули
- [monolithic-data](./monolithic-data) - модель данных и репозитории
- [monolithic-services](./monolithic-services) - сервисы
- [monolithic-bpm-engine](./monolithic-bpm) - BPM-модуль
- [monolithic-web](./monolithic-web) - WEB-модуль с выделенным REST-API
- [monolithic-runner](./monolithic-runner) - модуль запуска приложения

