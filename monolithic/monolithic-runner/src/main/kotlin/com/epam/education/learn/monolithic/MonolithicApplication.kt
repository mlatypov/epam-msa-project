package com.epam.education.learn.monolithic

import com.epam.education.learn.monolithic.model.Course
import com.epam.education.learn.monolithic.model.Person
import com.epam.education.learn.monolithic.repository.CourseRepository
import com.epam.education.learn.monolithic.repository.PersonRepository
import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication
@EnableProcessApplication
class MonolithicApplication {
    @Bean
    fun loadCourses(courseRepository: CourseRepository): CommandLineRunner {
        return CommandLineRunner {
            courseRepository.save(Course(
                    id = 0,
                    name = "Microservices Architecture Intensive #5",
                    description = """
                        Crash course on "Microservices architecture approach" fills in gaps in theoretical knowledge and practical experience for existing team members and candidates that are considered for new positions in Microservices based projects.
                    """.trimIndent(),
                    coachesIds = listOf(1, 2)
            ))
        }
    }

    @Bean
    fun loadPersons(personRepository: PersonRepository): CommandLineRunner {
        return CommandLineRunner {
            with(personRepository) {
                save(Person(
                        id = 1,
                        name = "Coach The First",
                        email = "first_coach@test.com"
                ))
                save(Person(
                        id = 2,
                        name = "Coach The Second",
                        email = "second_coach@test.com"
                ))

                save(Person(
                        id = 3,
                        name = "Ivan Ivanov",
                        email = "ivan_ivanov@test.com"
                ))

                save(Person(
                        id = 4,
                        name = "Petr Petrov",
                        email = "petr_petrov@test.com"
                ))
            }
        }
    }
}

fun main(args: Array<String>) {
    runApplication<MonolithicApplication>(*args)
}
