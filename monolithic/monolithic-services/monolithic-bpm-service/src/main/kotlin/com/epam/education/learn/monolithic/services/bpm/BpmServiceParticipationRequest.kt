package com.epam.education.learn.monolithic.services.bpm

import com.epam.education.learn.monolithic.bpm.constants.Variables
import com.epam.education.learn.monolithic.model.ParticipationRequest
import org.camunda.bpm.engine.RuntimeService
import org.camunda.bpm.engine.TaskService
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class BpmServiceParticipationRequest(
        private val runtimeService: RuntimeService,
        private val taskService: TaskService
) : BpmService<ParticipationRequest> {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun businessKey(request: ParticipationRequest) = "${processDefinitionKey()}_${request.id}"

    override fun startProcess(request: ParticipationRequest) {
        val businessKey = businessKey(request)

        // check if already exists
        runtimeService.createProcessInstanceQuery()
                .processInstanceBusinessKey(businessKey)
                .singleResult()?.let {
                    throw RuntimeException("process with business key ${businessKey} already exists")
                }

        val processInstance = runtimeService.createProcessInstanceByKey(processDefinitionKey())
                .businessKey(businessKey)
                .setVariable(Variables.ParticipationRequest.REQUEST_ID, request.id)
                .setVariable(Variables.ParticipationRequest.COURSE_ID, request.courseId)
                .setVariable(Variables.ParticipationRequest.INITIATOR_ID, request.initiatorId)
                .executeWithVariablesInReturn()
        logger.info("participation request process started, business key: {}, process instance id: {}, variables: {}",
                processInstance.businessKey, processInstance.processInstanceId, processInstance.variables
        )
    }

    override fun makeDecision(request: ParticipationRequest, decision: String) {
        logger.info("makeDecision: decision = {}", decision)
        val businessKey = businessKey(request)

        val task = taskService.createTaskQuery()
                .processInstanceBusinessKey(businessKey)
                .singleResult()
        taskService.setVariable(task.id, Variables.ParticipationRequest.MANAGER_DECISION,
                decision)
        val variableMap = taskService.completeWithVariablesInReturn(task.id, null, false)
        logger.info("task completed, business key: {}, process instance id: {}, variables: {}",
                businessKey, task.processInstanceId, variableMap)
    }

    override fun processDefinitionKey() = "participationRequest"
    override fun getState(context: ParticipationRequest): String {
        logger.info("getState: request id = {}", context.id)
        val businessKey = businessKey(context)

        var execution = runtimeService.createExecutionQuery()
                .processInstanceBusinessKey(businessKey)
                .singleResult()
        return runtimeService.getVariable(execution.id, "state").toString()
    }

}