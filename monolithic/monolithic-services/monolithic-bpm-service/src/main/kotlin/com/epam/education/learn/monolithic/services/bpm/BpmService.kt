package com.epam.education.learn.monolithic.services.bpm

/**
 * Service to work with BPM Engine
 */
interface BpmService<T> {

    /**
     * starts a process by a [context] object
     */
    fun startProcess(context: T)

    /**
     * starts a new process or change state of an existing
     */
    fun startOrUpdateProcess(context: T, variables: Map<String, Any>) {
        throw NotImplementedError()
    }

    /**
     * makes a [decision] by a [context] object
     */
    fun makeDecision(context: T, decision: String)

    /**
     * returns the key of the process definition
     */
    fun processDefinitionKey(): String

    /**
     * returns business key for the [context] object
     */
    fun businessKey(context: T): String

    /**
     * returns a state of the process of the context object
     */
    fun getState(context: T): String

}
