package com.epam.education.learn.monolithic.services.bpm

import com.epam.education.learn.monolithic.bpm.constants.Variables
import com.epam.education.learn.monolithic.model.Training
import org.camunda.bpm.engine.RuntimeService
import org.camunda.bpm.engine.TaskService
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class BpmServiceTraining(
        private val runtimeService: RuntimeService,
        private val taskService: TaskService
) : BpmService<Training> {

    private val logger = LoggerFactory.getLogger(javaClass)

    private fun isFull(training: Training): Boolean {
        return training.participantsIds.size == 2
    }

    override fun startProcess(training: Training) {
        val businessKey = businessKey(training)

        val processInstance = runtimeService.createProcessInstanceByKey(processDefinitionKey())
                .businessKey(businessKey)
                .setVariable(Variables.CourseProcess.COURSE_PROCESS_ID, training.id)
                .executeWithVariablesInReturn()
        logger.info("course process started, business key: {}, process instance id: {}, variables: {}",
                processInstance.businessKey, processInstance.processInstanceId, processInstance.variables)

    }

    override fun makeDecision(context: Training, decision: String) {
        logger.info("makeDecision: decision = {}", decision)
        val businessKey = businessKey(context)

        val task = taskService.createTaskQuery()
                .processInstanceBusinessKey(businessKey)
                .singleResult()
        taskService.setVariable(task.id, Variables.ParticipationRequest.MANAGER_DECISION,
                decision)
        val variableMap = taskService.completeWithVariablesInReturn(task.id, null, false)
        logger.info("task completed, business key: {}, process instance id: {}, variables: {}",
                businessKey, task.processInstanceId, variableMap)
    }

    override fun processDefinitionKey() = "training"

    override fun businessKey(training: Training) = "${processDefinitionKey()}_${training.id}"

    override fun startOrUpdateProcess(training: Training, variables: Map<String, Any>) {
        val businessKey = businessKey(training)

        // check if already exists
        val singleResult = runtimeService.createProcessInstanceQuery()
                .processInstanceBusinessKey(businessKey)
                .singleResult()
        singleResult?.let {processInstance ->
            val task = taskService.createTaskQuery()
                    .processInstanceBusinessKey(businessKey)
                    .singleResult()
            val variableMap = taskService.completeWithVariablesInReturn(task.id, null, false)
            logger.info("task completed, business key: {}, process instance id: {}, variables: {}",
                    businessKey, task.processInstanceId, variableMap)
        } ?: startProcess(training)

    }

    override fun getState(context: Training): String {
        logger.info("getState: training id = {}", context.id)
        val businessKey = businessKey(context)
        var execution = runtimeService.createExecutionQuery()
                .processInstanceBusinessKey(businessKey)
                .singleResult()
        return runtimeService.getVariable(execution.id, "state").toString()
    }
}