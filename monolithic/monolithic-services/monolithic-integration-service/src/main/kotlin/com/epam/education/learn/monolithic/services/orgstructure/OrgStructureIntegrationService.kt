package com.epam.education.learn.monolithic.services.orgstructure

/**
 * Service to integration with the structure of an organization
 */
interface OrgStructureIntegrationService {

    /**
     * get a manager's email from an organization structure by email of employee [person]
     */
    fun getManagerEmail(personId: Long): String

}