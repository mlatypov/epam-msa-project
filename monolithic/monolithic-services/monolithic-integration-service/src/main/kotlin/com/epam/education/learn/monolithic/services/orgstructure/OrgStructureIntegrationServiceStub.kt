package com.epam.education.learn.monolithic.services.orgstructure

import org.springframework.stereotype.Service

@Service
class OrgStructureIntegrationServiceStub : OrgStructureIntegrationService {
    override fun getManagerEmail(personId: Long): String {
        return "testmanager@test.com"
    }
}