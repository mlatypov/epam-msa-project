package com.epam.education.learn.monolithic.services.request

import com.epam.education.learn.monolithic.bpm.constants.Decisions
import com.epam.education.learn.monolithic.model.ParticipationRequest
import com.epam.education.learn.monolithic.repository.PersonRepository
import com.epam.education.learn.monolithic.repository.RequestRepository
import com.epam.education.learn.monolithic.services.bpm.BpmService
import com.epam.education.learn.monolithic.services.course.TrainingService
import org.springframework.stereotype.Service

@Service
class RequestServiceImpl(
        private val requestRepository: RequestRepository,
        private val personRepository: PersonRepository,
        private val bpmService: BpmService<ParticipationRequest>,
        private val trainingService: TrainingService
) : RequestService {


    override fun start(request: ParticipationRequest): ParticipationRequest {
        saveRequest(request)

        bpmService.startProcess(request)
        return request
    }

    override fun approve(request: ParticipationRequest) {
        bpmService.makeDecision(request, Decisions.ParticipationRequest.APPROVE.toString())
    }

    override fun decline(request: ParticipationRequest) {
        bpmService.makeDecision(request, Decisions.ParticipationRequest.DECLINE.toString())
    }

    override fun sendToCourseProcess(request: ParticipationRequest) {
        val person = personRepository.getOne(request.id)
        trainingService.addParticipantToCourseProcess(request.courseId, person)
    }

    override fun setFinalState(request: ParticipationRequest, state: ParticipationRequest.FinalState) {
        request.finalState = state
        requestRepository.save(request)
    }

    override fun findRequestById(id: Long): ParticipationRequest = requestRepository.getOne(id)

    override fun saveRequest(request: ParticipationRequest) {
        requestRepository.save(request)
    }

    override fun getStateById(id: Long): String {
        val request = findRequestById(id)
        if (request.finalState == null) {
            return bpmService.getState(request);
        } else if(request.finalState == ParticipationRequest.FinalState.DECLINED) {
            return request.finalState.toString()
        } else {
            return trainingService.getStateByCourseId(request.courseId)
        }
    }
}