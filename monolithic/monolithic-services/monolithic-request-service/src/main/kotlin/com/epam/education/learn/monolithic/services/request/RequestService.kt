package com.epam.education.learn.monolithic.services.request

import com.epam.education.learn.monolithic.model.ParticipationRequest

/**
 * Service to work with a participation request
 */
interface RequestService {

    /**
     * Start a process of an approval participation [request]
     */
    fun start(request: ParticipationRequest): ParticipationRequest

    /**
     * Approve a participation [request]
     */
    fun approve(request: ParticipationRequest)
    /**
     * Decline a participation [request]
     */
    fun decline(request: ParticipationRequest)
    /**
     * Send [request] to course process
     */
    fun sendToCourseProcess(request: ParticipationRequest)

    /**
     * Finds a participation request by an unique [id]
     */
    fun findRequestById(id: Long): ParticipationRequest

    /**
     * Stores a [request]
     */
    fun saveRequest(request: ParticipationRequest)

    /**
     * returns a state of the [ParticipationRequest] its [id]
     */
    fun getStateById(id: Long): String
    fun setFinalState(request: ParticipationRequest, state: ParticipationRequest.FinalState)

}