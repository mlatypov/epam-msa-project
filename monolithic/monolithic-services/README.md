# Сервисы

## Модули
- [monolithic-course-service](./monolithic-services) - сервис работы с курсами
- [monolithic-request-service](./monolithic-runner) - сервис работы с заявками на участие
- [monolithic-bpm-service](./monolithic-bpm-service) - сервис работы с BPM
- [monolithic-notification-service](./monolithic-web) - сервис уведомлений
- [monolithic-integration-service](./monolithic-integration-service) - интеграционные сервисы

