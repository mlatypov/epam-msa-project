package com.epam.education.learn.monolithic.services.notification

import org.slf4j.LoggerFactory
import org.slf4j.LoggerFactory.getLogger
import org.springframework.stereotype.Service

@Service
class NotificationServiceImpl : NotificationService {

    private val logger = getLogger(javaClass)

    override fun sendMail(address: String, template: String, additionalParams: Map<String, String>) {
        logger.error("sendMail: address={}, template={}, params={}", address, template, additionalParams)
    }
}