package com.epam.education.learn.monolithic.services.notification

/**
 * Service to send notifications
 */
interface NotificationService {

    /**
     * Send e-mail to the [address] by specified [template]. If an additional information is necessary it should be specified in [additionalParams]
     */
    fun sendMail(address: String, template: String, additionalParams: Map<String, String>)

}