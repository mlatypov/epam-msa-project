package com.epam.education.learn.monolithic.services.course

import com.epam.education.learn.monolithic.bpm.constants.Decisions
import com.epam.education.learn.monolithic.model.Person
import com.epam.education.learn.monolithic.model.Training
import com.epam.education.learn.monolithic.repository.TrainingRepository
import com.epam.education.learn.monolithic.repository.CourseRepository
import com.epam.education.learn.monolithic.services.bpm.BpmService
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.lang.IllegalArgumentException
import java.lang.RuntimeException

@Service
class TrainingServiceImpl(
        private val trainingRepository: TrainingRepository,
        private val courseRepository: CourseRepository,
        private val bpmService: BpmService<Training>
) : TrainingService {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun addParticipantToCourseProcess(courseId: Long, participant: Person) {
        val training = trainingRepository.findByCourseId(courseId)?: Training(
                id = 0,
                course = courseRepository.getOne(courseId),
                participantsIds = mutableListOf(),
                state = Training.State.WAIT_PARTICIPANTS
        )

        if (training.state != Training.State.WAIT_PARTICIPANTS) {
            throw IllegalArgumentException("The list of participants is full")
        }

        training.participantsIds.add(participant.id)
        val storedTraining = trainingRepository.save(training)
        bpmService.startOrUpdateProcess(storedTraining, emptyMap())

    }

    override fun getActiveTrainingByCourseId(courseId: Long): Training {
        return trainingRepository.findByCourseIdAndStateIsNot(courseId, Training.State.FINISHED)
                ?: throw IllegalArgumentException()
    }

    override fun findTrainingById(id: Long): Training {
        return trainingRepository.getOne(id)
    }

    override fun approve(training: Training) {
        bpmService.makeDecision(training, Decisions.CourseProcess.APPROVE.toString())
    }

    override fun decline(training: Training) {
        bpmService.makeDecision(training, Decisions.CourseProcess.DECLINE.toString())
    }

    override fun getStateByCourseId(courseId: Long): String {
        val training = trainingRepository.findByCourseIdAndStateIsNot(courseId, Training.State.FINISHED)?.let {
            return bpmService.getState(it)
        }
        throw RuntimeException("Training not found")
    }
}