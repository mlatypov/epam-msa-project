package com.epam.education.learn.monolithic.services.course

import com.epam.education.learn.monolithic.model.Person
import com.epam.education.learn.monolithic.model.Training

interface TrainingService {

    /**
     * add participant to the course process by [courseId]
     */
    fun addParticipantToCourseProcess(courseId: Long, participantEmail: Person)
    fun findTrainingById(id: Long): Training
    fun approve(training: Training)
    fun decline(training: Training)
    fun getActiveTrainingByCourseId(courseId: Long): Training
    fun getStateByCourseId(courseId: Long): String

}