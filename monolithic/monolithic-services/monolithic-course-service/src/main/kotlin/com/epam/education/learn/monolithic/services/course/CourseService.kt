package com.epam.education.learn.monolithic.services.course

import com.epam.education.learn.monolithic.model.Course

/**
 * Service to work with courses
 */
interface CourseService {

    /**
     * creates a new course
     */
    fun create(course: Course): Course

    /**
     * finds a course by an unique id
     */
    fun findById(id: Long): Course
}