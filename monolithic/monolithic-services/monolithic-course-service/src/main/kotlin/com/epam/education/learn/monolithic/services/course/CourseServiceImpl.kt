package com.epam.education.learn.monolithic.services.course

import com.epam.education.learn.monolithic.model.Course
import com.epam.education.learn.monolithic.repository.CourseRepository
import org.springframework.stereotype.Service

@Service
class CourseServiceImpl(
        private val courseRepository: CourseRepository
) : CourseService {
    override fun create(course: Course): Course {
        return courseRepository.save(course)
    }

    override fun findById(id: Long): Course {
        return courseRepository.getOne(id)
    }
}