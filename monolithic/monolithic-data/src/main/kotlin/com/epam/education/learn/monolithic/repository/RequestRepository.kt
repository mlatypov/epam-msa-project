package com.epam.education.learn.monolithic.repository

import com.epam.education.learn.monolithic.model.ParticipationRequest
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface RequestRepository: JpaRepository<ParticipationRequest, Long>
