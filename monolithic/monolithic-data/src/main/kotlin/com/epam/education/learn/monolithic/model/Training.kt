package com.epam.education.learn.monolithic.model

import javax.persistence.ElementCollection
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.OneToMany

@Entity
data class Training(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,
        @ManyToOne
        val course: Course,
        @ElementCollection
        val participantsIds: MutableList<Long>,
        var state: State = State.WAIT_PARTICIPANTS
) {
        enum class State {
                WAIT_PARTICIPANTS,
                APPROVAL,
                STARTED,
                FINISHED
        }
}