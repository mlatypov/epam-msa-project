package com.epam.education.learn.monolithic.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
class ParticipationRequest(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,
        val initiatorId: Long = 0,
        val courseId: Long = 0,
        var finalState: FinalState? = null
) {
        enum class FinalState {
                APPROVED,
                DECLINED
        }
}