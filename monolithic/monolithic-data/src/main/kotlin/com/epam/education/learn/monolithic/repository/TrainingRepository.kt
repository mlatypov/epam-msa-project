package com.epam.education.learn.monolithic.repository

import com.epam.education.learn.monolithic.model.Training
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface TrainingRepository : JpaRepository<Training, Long> {
    fun findByCourseId(id: Long): Training?
    fun findByCourseIdAndState(id: Long, state: Training.State): Training?
    fun findByCourseIdAndStateIsNot(id: Long, state: Training.State): Training?
}