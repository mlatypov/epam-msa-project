package com.epam.education.learn.monolithic.web.rest

import com.epam.education.learn.monolithic.model.Training
import com.epam.education.learn.monolithic.services.course.TrainingService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/trainings")
class TrainingController(
        private val trainingService: TrainingService
) {

    @PostMapping("/approve/{id}")
    fun approve(@PathVariable id: Long) {
        trainingService.run {
            approve(findTrainingById(id))
        }
    }

    @PostMapping("/decline/{id}")
    fun decline(@PathVariable id: Long) {
        trainingService.run {
            decline(findTrainingById(id))
        }
    }

    @GetMapping("/{id}")
    fun findById(@PathVariable id: Long): Training {
        return trainingService.findTrainingById(id)
    }

}