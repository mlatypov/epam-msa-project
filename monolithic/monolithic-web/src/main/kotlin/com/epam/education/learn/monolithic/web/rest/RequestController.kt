package com.epam.education.learn.monolithic.web.rest

import com.epam.education.learn.monolithic.model.ParticipationRequest
import com.epam.education.learn.monolithic.services.request.RequestService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/requests")
class RequestController(
        private val requestService: RequestService
) {

    @PostMapping("/start")
    fun start(@RequestBody request: ParticipationRequest): ParticipationRequest {
        return requestService.start(request)
    }

    @PostMapping("/approve/{id}")
    fun approve(@PathVariable id: Long) {
        requestService.run {
            approve(findRequestById(id))
        }
    }

    @PostMapping("/decline/{id}")
    fun decline(@PathVariable id: Long) {
        requestService.run {
            decline(findRequestById(id))
        }
    }

    @GetMapping("/state/{id}")
    fun state(@PathVariable id: Long): String {
        return requestService.getStateById(id)
    }

    @GetMapping("/{id}")
    fun findById(@PathVariable id: Long): ParticipationRequest {
        return requestService.findRequestById(id)
    }

}