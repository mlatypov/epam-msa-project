package com.epam.education.learn.monolithic.web.rest

import com.epam.education.learn.monolithic.model.Course
import com.epam.education.learn.monolithic.model.Training
import com.epam.education.learn.monolithic.services.course.CourseService
import com.epam.education.learn.monolithic.services.course.TrainingService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/courses")
class CourseController(
        private val courseService: CourseService,
        private val trainingService: TrainingService
){

    @PostMapping
    fun create(@RequestBody course: Course): Course {
        return courseService.create(course)
    }

    @GetMapping("/{id}")
    fun findById(@PathVariable id: Long): Course {
        return courseService.findById(id)
    }

    @GetMapping("/{courseId}/activeTraining")
    fun state(@PathVariable courseId: Long): Training {
        return trainingService.getActiveTrainingByCourseId(courseId)
    }

}