package com.epam.education.learn.monolithic.bpm.constants

/**
 * Names of variables in a BPM-process
 */
object Variables {

    /**
     * Names of variables in the participation request process
     */
    object ParticipationRequest {
        const val MANAGER_DECISION = "managerDecision"
        const val COURSE_ID = "courseId"
        const val REQUEST_ID = "requestId"
        const val INITIATOR_ID = "initiatorId"
    }

    object CourseProcess {
        const val IS_FULL = "isFull"
        const val COURSE_PROCESS_ID = "courseProcessId"
    }

}