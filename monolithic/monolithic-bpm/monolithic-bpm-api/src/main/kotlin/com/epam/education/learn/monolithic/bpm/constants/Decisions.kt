package com.epam.education.learn.monolithic.bpm.constants

/**
 * Decisions
 */
object Decisions {
    /**
     * Decisions of the participation request process
     */
    enum class ParticipationRequest {
        APPROVE,
        DECLINE
    }

    enum class CourseProcess {
        APPROVE,
        DECLINE
    }
}