package com.epam.education.learn.monolithic.bpm.tasks

import com.epam.education.learn.monolithic.bpm.constants.Variables
import com.epam.education.learn.monolithic.repository.PersonRepository
import com.epam.education.learn.monolithic.services.course.CourseService
import com.epam.education.learn.monolithic.services.notification.NotificationService
import com.epam.education.learn.monolithic.services.orgstructure.OrgStructureIntegrationService
import com.epam.education.learn.monolithic.services.request.RequestService
import org.camunda.bpm.engine.impl.pvm.delegate.ActivityExecution
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
class BpmNotificationService(
        private val notificationService: NotificationService,
        private val personRepository: PersonRepository,
        private val orgStructureIntegrationService: OrgStructureIntegrationService,
        private val courseService: CourseService
) {

    private val logger = LoggerFactory.getLogger(javaClass)

    fun sendMailToInitiator(execution: ActivityExecution, template: String) {
        logger.info("sendMailToInitiator, template = {}",
                template)

        val initiatorId = execution.getLongVariable(Variables.ParticipationRequest.INITIATOR_ID)
        val initiator = personRepository.getOne(initiatorId)

        val courseId = execution.getLongVariable(Variables.ParticipationRequest.COURSE_ID)
        val course = courseService.findById(courseId)

        notificationService.sendMail(
                address = initiator.email,
                template = template,
                additionalParams =  mapOf(
                        "initiatorName" to initiator.name,
                        "courseDescription" to course.description,
                        "courseName" to course.name
                ))
    }

    fun sendMailToInitiatorManager(execution: ActivityExecution, template: String) {
        logger.info("sendMailToInitiatorManager, template = {}",
                template)

        val initiatorId = execution.getLongVariable(Variables.ParticipationRequest.INITIATOR_ID)
        val initiator = personRepository.getOne(initiatorId)

        val courseId = execution.getLongVariable(Variables.ParticipationRequest.COURSE_ID)
        val course = courseService.findById(courseId)

        val managerEmail = orgStructureIntegrationService.getManagerEmail(initiatorId)

        notificationService.sendMail(
                address = managerEmail,
                template = template,
                additionalParams =  mapOf(
                        "initiatorName" to initiator.name,
                        "courseDescription" to course.description,
                        "courseName" to course.name
                ))
    }

    fun sendMailByCourseProcess(execution: ActivityExecution, email: String, template: String) {
        logger.info("sendMailByCourseProcess, email = {}, template = {}",
                email, template)

        notificationService.sendMail(
                address = email,
                template = template,
                additionalParams = emptyMap()
        )
    }
}