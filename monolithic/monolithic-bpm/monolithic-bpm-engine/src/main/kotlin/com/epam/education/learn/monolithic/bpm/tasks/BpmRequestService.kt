package com.epam.education.learn.monolithic.bpm.tasks

import com.epam.education.learn.monolithic.bpm.constants.Variables
import com.epam.education.learn.monolithic.model.ParticipationRequest
import com.epam.education.learn.monolithic.services.request.RequestService
import org.camunda.bpm.engine.impl.pvm.delegate.ActivityExecution
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
class BpmRequestService(
        private val requestService: RequestService
) {

    private val logger = LoggerFactory.getLogger(javaClass)

    fun setState(execution: ActivityExecution, state: String) {
        val requestId = execution.getLongVariable(Variables.ParticipationRequest.REQUEST_ID)
        val request = requestService.findRequestById(requestId)
        requestService.setFinalState(request, ParticipationRequest.FinalState.valueOf(state.toUpperCase()))
    }

    fun sendToCourseProcess(execution: ActivityExecution) {
        val requestId = execution.getLongVariable(Variables.ParticipationRequest.REQUEST_ID)
        requestService.run {
            sendToCourseProcess(findRequestById(requestId))
        }
    }
}