package com.epam.education.learn.monolithic.bpm.tasks

import org.camunda.bpm.engine.impl.pvm.delegate.ActivityExecution

fun ActivityExecution.getStringVariable(name: String): String {
    return getVariable(name).toString()
}

fun ActivityExecution.getLongVariable(name: String): Long {
    return getVariable(name).toString().toLong()
}