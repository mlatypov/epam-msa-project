plugins {
    kotlin("jvm") version "1.3.72"
}

group = "com.epam.education.learn"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    testImplementation ("io.rest-assured:rest-assured:4.3.0")
    testImplementation ("io.rest-assured:kotlin-extensions:4.3.0")
    testImplementation("org.junit.jupiter:junit-jupiter:5.6.2")

}


tasks.withType<Test> {
    useJUnitPlatform()
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}