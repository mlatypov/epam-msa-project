import io.restassured.builder.RequestSpecBuilder
import io.restassured.http.ContentType
import io.restassured.module.kotlin.extensions.Extract
import io.restassured.module.kotlin.extensions.Given
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import io.restassured.response.Response
import io.restassured.response.ValidatableResponse
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.not
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Test

const val BASE_URI = "http://localhost:8000"
const val REQUESTS_URI = "${BASE_URI}/requests"
const val COURSES_URI = "${BASE_URI}/courses"
const val TRAININGS_URI = "${BASE_URI}/trainings"

val specBase = RequestSpecBuilder()
        .setBaseUri(BASE_URI)
        .build()


val specPost = RequestSpecBuilder()
        .addRequestSpecification(specBase)
        .setContentType(ContentType.JSON)
        .build()


class ApiTest {


    @Test
    fun testCreateCourseWithSameId() {
        val firstCourseId = createCourse() Extract {
            path<Int>("id")
        }

        val secondCourseId = createCourse() Extract {
            path<Int>("id")
        }

        assertThat(secondCourseId, not(firstCourseId))
    }

    @Test
    fun testSuccessRequest() {
        val courseId = createCourse().extract().path<Int>("id")
        val firstRequestId = startRequest(courseId, 3)
                .extract()
                .path<Int>("id")
        assertThat(requestState(firstRequestId), equalTo("managerApproval"))
        approveRequest(firstRequestId)
        assertThat(requestState(firstRequestId), equalTo("waitingForParticipants"))

        val secondRequestId = startRequest(courseId, 4)
                .extract()
                .path<Int>("id")
        approveRequest(secondRequestId)
        assertThat(requestState(firstRequestId), equalTo("COACHES_APPROVAL"))
        assertThat(requestState(secondRequestId), equalTo("COACHES_APPROVAL"))

        val trainingId = activeTrainingByCourseId(courseId) Extract {
            path<Int>("id")
        }

        approveTraining(trainingId)
        assertThat(requestState(firstRequestId), equalTo("TRAINING"))
        assertThat(requestState(secondRequestId), equalTo("TRAINING"))
    }

    @Test
    fun testDeclineRequest() {
        val courseId = createCourse()
                .extract()
                .path<Int>("id")
        val requestId = startRequest(courseId, 3)
                .extract()
                .path<Int>("id")
        assertThat(requestState(requestId), equalTo("managerApproval"))
        declineRequest(requestId)
        assertThat(requestState(requestId), equalTo("DECLINED"))
    }

    private fun approveTraining(trainingId: Int) {
        Given {
            spec(specPost)
        } When {
            post("$TRAININGS_URI/approve/{requestId}", trainingId )
        }
    }

    private fun declineTraining(trainingId: Int) {
        Given {
            spec(specPost)
        } When {
            post("$TRAININGS_URI/approve/{requestId}", trainingId )
        }
    }

    private fun activeTrainingByCourseId(courseId: Int): Response {
        return Given {
            spec(specBase)
        } When {
            get("$COURSES_URI/{courseId}/activeTraining", courseId )
        }
    }

    private fun requestState(requestId: Int): String {
        return Given {
            spec(specBase)
        } When {
            get("$REQUESTS_URI/state/{requestId}", requestId )
        } Extract {
            asString()
        }
    }

    private fun approveRequest(requestId: Int) {
        Given {
            spec(specPost)
        } When {
            post("$REQUESTS_URI/approve/{requestId}", requestId )
        }
    }

    private fun declineRequest(requestId: Int) {
        Given {
            spec(specPost)
        } When {
            post("$REQUESTS_URI/decline/{requestId}", requestId )
        }
    }

    private fun startRequest(courseId: Int, initiatorId: Int): ValidatableResponse {
        return Given {
            spec(specPost).body("""
                    {
                        "initiatorId": "$initiatorId",
                        "courseId": "$courseId"
                    }
                """.trimIndent()
            )
        } When {
            post("$REQUESTS_URI/start")
        } Then {
            body(
                    "courseId", equalTo(courseId)
            )
        }
    }
    private fun createCourse(): ValidatableResponse {
        return Given {
            spec(specPost).body("""
                    {
                        "name": "Test course",
                        "description": "some words about the course",
                        "coachesIds": [1, 2]
                    }
                """.trimIndent()
            )
        } When {
            post(COURSES_URI)
        } Then {
            body(
                    "name", equalTo("Test course"),
                    "description", equalTo("some words about the course")
            )
        }
    }
}